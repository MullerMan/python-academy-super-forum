from django.contrib.messages import get_messages
from django.test import Client, TestCase
from django.urls import reverse_lazy

from forum.common.test_mixins import TestMixin
from forum.common.models import Category


class TestIndexView(TestMixin, TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = Client()

    @staticmethod
    def get_category_form_values():
        return {'name': 'Test category', 'order_number': 1}

    def test_index_without_categories(self):
        response = self.client.get(reverse_lazy('common:index'))
        self.assertContains(response, 'No categories', status_code=200)

    def test_is_user_anonymous(self):
        response = self.client.get(reverse_lazy('common:index'))
        self.assertContains(response, 'Login', status_code=200)
        self.assertContains(response, 'Register')

    def test_is_user_logged(self):
        user = self.get_or_create_user()
        self.client.force_login(user)
        response = self.client.get(reverse_lazy('common:index'))
        self.assertContains(response, 'Profile', status_code=200)

    def test_user_does_not_see_add_category_button(self):
        user = self.get_or_create_user()
        self.client.force_login(user)
        response = self.client.get(reverse_lazy('common:index'))
        self.assertNotContains(response, 'Create category', status_code=200)

    def test_superuser_see_add_category_button(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)
        response = self.client.get(reverse_lazy('common:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'Create category')

    def test_user_cannot_load_category_form(self):
        user = self.get_or_create_user()
        self.client.force_login(user)
        response = self.client.get(reverse_lazy('common:create_category'))
        self.assertContains(response, '403 Forbidden', status_code=403)

    def test_superuser_can_load_category_form(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)
        response = self.client.get(reverse_lazy('common:create_category'))
        self.assertContains(response, 'Category name', status_code=200)
        self.assertContains(response, 'Order number')

    def test_user_cannot_create_category(self):
        user = self.get_or_create_user()
        data = self.get_category_form_values()
        self.client.force_login(user)
        response = self.client.post(reverse_lazy('common:create_category'), data)
        self.assertContains(response, '403 Forbidden', status_code=403)

    def test_superuser_can_create_category(self):
        user = self.get_or_create_superuser()
        data = self.get_category_form_values()
        self.client.force_login(user)

        response = self.client.post(reverse_lazy('common:create_category'), data)
        category = Category.objects.get(**data)
        messages = list(get_messages(response.wsgi_request))

        self.assertEqual(302, response.status_code)
        self.assertIsNotNone(category)
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Created category.')

    def test_create_category_validate_data(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)
        data = {'name': self.faker.pystr(max_chars=2)}

        response = self.client.post(reverse_lazy('common:create_category'), data)
        self.assertContains(response, 'Category name to short.')
        self.assertContains(response, 'This field is required.')
        with self.assertRaises(Category.DoesNotExist):
            Category.objects.get(**data)

        data = {
            'name': self.faker.pystr(min_chars=17),
            'order_number': self.faker.pyint(min_value=-9999, max_value=-1)
        }
        name_length = len(data['name'])

        response = self.client.post(reverse_lazy('common:create_category'), data)
        self.assertContains(response, f'Ensure this value has at most 16 characters (it has {name_length}).')
        self.assertContains(response, 'Ensure this value is greater than or equal to 0.')
        with self.assertRaises(Category.DoesNotExist):
            Category.objects.get(**data)
