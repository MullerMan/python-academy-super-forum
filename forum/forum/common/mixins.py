from typing import Optional, cast

from django.contrib.auth.mixins import UserPassesTestMixin


class SuperUserMixin(UserPassesTestMixin):
    """ Mixin to check user is super user """
    def test_func(self) -> Optional[bool]:
        return cast(bool, self.request.user.is_superuser)  # type: ignore[attr-defined]
