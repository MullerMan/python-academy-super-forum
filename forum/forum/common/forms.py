from django.forms import (
    CharField,
    EmailField,
    ModelForm,
)
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm

from forum.common.models import (
    Category,
    Topic,
    Post,
)


class CategoryCreateForm(ModelForm):
    """ Create category form """

    class Meta:
        """ Meta class """
        model = Category
        fields = ['name', 'order_number']


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['text']


class TopicForm(ModelForm):
    """ Create topic form """

    class Meta:
        model = Topic
        fields = ['name']


class RegisterForm(UserCreationForm):
    """ User registration form """
    first_name = CharField(max_length=30, required=False, help_text="Optional.")
    last_name = CharField(max_length=30, required=False, help_text="Optional.")
    email = EmailField(max_length=128, help_text="Required.")

    class Meta:
        """ Meta class """
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
