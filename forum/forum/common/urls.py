from django.urls import path

from forum.common.views import (
    create_view,
    index_view,
    update_view,
    delete_view,
    register_view,
    create_post_view,
    update_topic_view,
    block_topic_view,
    topic_list_view,
    topic_posts_view,
    topic_create_view,
    post_delete_view,
)

app_name = 'common'
urlpatterns = [
    path('', index_view, name='index'),
    path('category/create', create_view, name='create_category'),
    path('category/update/<int:pk>', update_view, name='update_category'),
    path('category/delete/<int:pk>', delete_view, name='delete_category'),
    path('topics/<int:pk>', topic_list_view, name='topics'),
    path('topic/create/<int:pk>', topic_create_view, name='create_topic'),
    path('topic/update/<int:pk>', update_topic_view, name='update_topic'),
    path('topic/block/<int:pk>', block_topic_view, name='block_topic'),
    path('topic/posts/<int:pk>', topic_posts_view, name='topic_posts'),
    path('create/post/<int:pk>', create_post_view, name='create_post'),
    path('post/delete/<int:pk>', post_delete_view, name='delete_post'),

    path('register', register_view, name='register'),
]
