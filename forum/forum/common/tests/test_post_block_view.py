from django.test import Client, TestCase
from django.urls import reverse_lazy

from forum.common.test_mixins import TestMixin
from forum.common.models import Category, Topic


class TestPostDeleteView(TestMixin, TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = Client()
        self.category, _ = Category.objects.get_or_create({
            'name': 'Test_category',
            'order_number': 1,
        })
        self.topic, _ = Topic.objects.get_or_create({
            'name': 'Test_topic',
            'category_id': self.category.pk,
        })

    def test_post_block_view_load_superuser(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertContains(response, 'Are you sure you want block topic', status_code=200)
        self.assertContains(response, user.username)

    def test_post_block_view_load_user(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertEqual(403, response.status_code)

    def test_post_block_view_user_loading(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        category = self.category

        response = self.client.get(reverse_lazy('common:topics', kwargs={'pk': category.pk}))
        self.assertNotContains(response, 'Block', status_code=200)

    def test_post_block_view_superuser_loading(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        category = self.category

        response = self.client.get(reverse_lazy('common:topics', kwargs={'pk': category.pk}))
        self.assertContains(response, 'Block', status_code=200)
