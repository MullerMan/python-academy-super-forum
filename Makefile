# Makefile
SRC_DIR = forum
SHELL := /bin/bash
PROJECT = PythonAcademyForum
VENV_DIR ?= venv

ifeq ($(OS),Windows_NT)
		PY_VERSION = python
else
		PY_VERSION = python3
endif


DEVELOPMENT_MODE=true
DATABASE_URL?=postgres://postgres:postgres@localhost:5432/forumdb

# PROJECT

init-env:
	(test `which virtualenv` || $(PIP_CMD) install --user virtualenv) && \
		(test -e $(VENV_DIR) || virtualenv $(VENV_OPTS) $(VENV_DIR)) && \
		($(PY_VERSION) -m pip install --upgrade pip)

create-env:
	test ! -e $(SRC_DIR)/requirements.txt || pip -q install -r $(SRC_DIR)/requirements.txt

freeze:
	pip freeze > $(SRC_DIR)/requirements.txt

clear-dependencies:
	pip freeze | xargs pip uninstall -y

# DJANGO

createsuperuser:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py createsuperuser --username=joe --email=joe@example.com

makemessages:
	cd $(SRC_DIR) && $(PY_VERSION) manage.py makemessages -l pl

compilemessages:
	cd $(SRC_DIR) && $(PY_VERSION) manage.py compilemessages -l pl

migrate:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py migrate

makemigrations:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py makemigrations

tests:
	chmod +x .cicd/scripts/run-tests.sh && \
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	PYTHONPATH=$(SRC_DIR) \
	../.cicd/scripts/run-tests.sh $(SRC_DIR)

# DOCKER

dev-env-up:
	docker volume create --name=db-pg-data
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml up -d --build --remove-orphans
	@while [ $$(docker ps --filter health=starting --format "{{.Status}}" | wc -l) != 0 ]; do echo 'waiting for healthy containers'; sleep 1; done

dev-env-down:
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml down -v --remove-orphans

dev-env-clean: dev-env-down
	docker volume remove db-pg-data

# UTILS

run-static-analysis:
	chmod +x .cicd/scripts/run_static_analysis.sh && \
	MYPYPATH="$(SRC_DIR)" .cicd/scripts/run_static_analysis.sh forum/forum/
