from gettext import gettext as _

from django.contrib.auth import get_user_model
from django.db import models
from django.core.validators import MinLengthValidator
from django.utils.text import slugify


def get_or_create_deleted():
    """ Get deleted user """
    return get_user_model().objects.get_or_create(name='Erased')[0]


class Category(models.Model):
    name = models.CharField(
        _('Category name'),
        max_length=16,
        validators=[
            MinLengthValidator(3, _('Category name to short.')),
        ]
    )
    order_number = models.PositiveIntegerField(
        _('Order number'),
        default=0,
    )

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['-order_number']

    def __str__(self):
        return self.name


class Topic(models.Model):
    name = models.CharField(
        _('Topic'),
        max_length=128,
        validators=[
            MinLengthValidator(12),
        ]
    )
    created_at = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(
        _('Active'),
        default=True,
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )

    class Meta:
        """ Meta class """
        verbose_name = _('Topic')
        verbose_name_plural = _('Topics')
        ordering = ['-created_at']

    def __str__(self):
        return slugify(self.name)


class Post(models.Model):
    text = models.TextField(_('Text'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_ad = models.DateTimeField(auto_now=True)
    topic = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET(get_or_create_deleted),
        null=True,
        blank=False,
    )

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['created_at']
