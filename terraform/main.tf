terraform {
  required_providers {
    heroku = {
      source  = "heroku/heroku"
      version = "~> 4.6.0"
    }
  }

  required_version = ">= 1.0.11"
}

resource "heroku_app" "python_academy_forum" {
  name   = var.app_name
  region = "eu"

  config_vars = {
    LOG_LEVEL = "info"
    DEBUG     = false
    VERSION   = "0.0.1"
  }
}

resource "heroku_addon" "postgres_database" {
  app  = heroku_app.python_academy_forum.id
  plan = var.postgres_plan
}

resource "heroku_build" "python_academy_forum" {
  app = heroku_app.python_academy_forum.id

  source {
    path    = var.app_path
    version = "0.0.1"
  }
}

resource "heroku_config" "common" {
  sensitive_vars = {
    SECRET_KEY = var.secret_key
  }
}

resource "heroku_formation" "app_formation" {
  app        = heroku_app.python_academy_forum.id
  quantity   = 1
  size       = "Free"
  type       = "web"
  depends_on = [heroku_build.python_academy_forum]
}