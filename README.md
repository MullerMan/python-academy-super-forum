# Own home [![pipeline status](https://gitlab.com/Draqun/django_training/badges/master/pipeline.svg)](https://gitlab.com/Draqun/django_training/-/commits/master) [![coverage report](https://gitlab.com/Draqun/django_training/badges/master/coverage.svg)](https://gitlab.com/Draqun/django_training/-/commits/master)

### Setup Database
- To make migrations run `./manage.py makemigrations`
- To setup migtrations run `./manage.py migrate`

### Authentication
- To use integrated authentication app add in urls.py `path('', include('django.contrib.auth.urls'))`
- To create admin account use command `./manage.py createsuperuser --username=joe --email=joe@example.com`

To resolve problems with redirection after login/logout add to `settings.py`
```python
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"
```

### Translation
For Translation for windows remember install

- http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/gettext-runtime-0.17-1.zip
- http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/gettext-tools-0.17.zip
- https://stackoverflow.com/questions/38806553/how-to-install-gnu-gettext-0-15-on-windows-so-i-can-produce-po-mo-files-in

For translations:
- Install gettext tools
```bash
sudo apt install gettext
``` 
- Add correct middleware
```python
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',  # This middleware should be added
    'django.middleware.common.CommonMiddleware',
```
- Create `locale` directory
- Add path to 'locale' directory in settings.py `LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'), )`
- Run `./manage.py makemessages -l pl` to create polish translation files
- Run `./manage.py compilemessages` to create polish translation files

### To publish on Heroku

- Create app on heroku.com (`staging` and `production`)
- From account settings get API Key
- In GitLab CI\CD setup `HEROKU_API_KEY` with values get from Heroku `HEROKU_APP_PRODUCTION` with production app name and `HEROKU_APP_STAGING` with staging name
- Generate secret keys for apps and setup them on the Heroku environment. To do it use:
```python
 from string import ascii_letters, digits, punctuation;import random; print(''.join(random.SystemRandom().choice(ascii_letters + digits + punctuation) for i in range(50)), end='')
```

In `settings.py` add next changes:

To avoid secret key leaks

```python
SECRET_KEY = environ["SECRET_KEY"] if "SECRET_KEY" in environ else uuid4()
```

To avoid leaks of sensitive data

```python
DEVELOPMENT_MODE = bool(strtobool(environ.get('DEVELOPMENT_MODE', 'False')))
```

To allow works on Heroku domains

```python
ALLOWED_HOSTS = ['you-hero-domain.com'] if not DEVELOPMENT_MODE else []
```

For static files

```python
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
```

Create Procfile with content like below

```text
release: python3 manage.py migrate --noinput
web: gunicorn own_home.wsgi
```

Create runtime.txt file with content
```text
python-3.10.0
```

For static files install whitenoise `pip install whitenoise==5.1.0` and setup it
```python
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whitenoise.runserver_nostatic',  #  To obfuscate code
    'bootstrap4',  # Bootstrap app
    'forum.common',  # your app
]
```

Install helper for Heroku database `pip install dj-database-url` and add in `settings.py`
```python
import dj_database_url
...

DATABASES = {
    'default': dj_database_url.config(conn_max_age=600, ssl_require=not DEVELOPMENT_MODE)  # On localhost in development mode ssl is not required
}
```

Remember also setup variable
```bash
export DATABASE_URL=postgres://postgres:postgres@localhost:5432/user_database_name
```

### Useful messages

In your views add correct mixin, before you add generic view class
```python
from django.contrib.messages.views import SuccessMessageMixin
...

class SuperCreateView(SuccessMessageMixin, ...):
...
```

In settings.py add. Variable `MESSAGE_TAGS` contains definition of bootstrap4 CSS classes 
```python
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'
MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}
```

Add in your application base template this part to print success messages

```html
{% if messages %}
    {% for message in messages %}
      <div class="alert {{ message.tags }} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        {{ message }}
      </div>
    {% endfor %}
{% endif %}
```

### Install Docker
```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker ${USER}
```

### Install terraform

https://learn.hashicorp.com/tutorials/terraform/install-cli

```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```

### Use dpl to deployment if you do not want work with terraform

Add to your `.gitlab-ci.yml`

```yaml
staging:
  stage: staging
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --cleanup --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY
  only:
    - master

production:
  stage: production
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --cleanup --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY
  only:
    - tags
```