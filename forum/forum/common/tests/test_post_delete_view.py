from django.test import Client, TestCase
from django.urls import reverse_lazy

from forum.common.test_mixins import TestMixin
from forum.common.models import Category, Topic, Post


class TestPostDeleteView(TestMixin, TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = Client()
        category, _ = Category.objects.get_or_create({
            'name': 'Test category',
            'order_number': 1,
        })
        self.topic, _ = Topic.objects.get_or_create({
            'name': 'This is test topic',
            'category_id': category.pk,
        })

    def get_post(self, **kwargs):
        kwargs_def = {
            'topic_id': self.topic.pk
        }
        kwargs_def.update(kwargs)
        return Post.objects.create(**kwargs_def)

    def test_post_delete_view_not_load_for_unauthenticated_user(self):
        user = self.get_or_create_user()
        post = self.get_post(author_id=user.pk, text=self.faker.text())
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertEqual(302, response.status_code)
        self.assertTrue('/login' in response.url)

    def test_post_delete_view_load_for_user(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        text = self.faker.text()
        post = self.get_post(author_id=user.pk, text=text)
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertContains(response, 'Are you sure you want delete post?', status_code=200)
        self.assertContains(response, user.username)
        self.assertContains(response, text)

    def test_post_delete_view_load_for_superuser(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        text = self.faker.text()
        post = self.get_post(author_id=user.pk, text=text)
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertContains(response, 'Are you sure you want delete post?', status_code=200)
        self.assertContains(response, user.username)
        self.assertContains(response, text)
